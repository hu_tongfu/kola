import Vue from 'vue'
//1.引入 axios
import Axios from 'axios'

const _self = new Vue();

//配置基础url
const instance = function (url, contentType, isCookie) {
    return Axios.create({
        baseURL: url,
        headers: {
            "Content-Type": contentType ? contentType : "application/x-www-form-urlencoded",
        },
        //配置超时
        timeout: 10000,
        responseType: "json",
        withCredentials: !!!isCookie, // 是否允许带cookie这些
        transformRequest: [function (data) {
            //转换请求方式，将请求的参数转换为 form-data
            let ret = '';
            for (let item in data) {
                ret += encodeURIComponent(item) + '=' + encodeURIComponent(data[item]) + '&'
            }
            return ret
        }],
        transformResponse: [function (data) {
            //转换data 不用每个请求都用 querystring.stringify
            return data;
        }]
    });
};
const ip = '192.168.100.22';
//url
export const WSS_URL = "ws://" + ip + ":8003/websocket/";
export const BASE_URL = "http://" + ip + "/b/";
export const IM_URL = "http://" + ip + "/i/";
export const COMMON_URL = "http://" + ip + "/c/";
export const FLOW_URL = "http://" + ip + "/w/";
//创建 axios实例
export const axiosBaseInstance = instance(BASE_URL);
export const axiosBaseInstanceJson = instance(BASE_URL, "application/json;charset=UTF-8");
export const axiosIMInstance = instance(IM_URL);
export const axiosCommonInstance = instance(COMMON_URL);
export const axiosFlowInstance = instance(FLOW_URL);

//配置  http request 拦截器
axiosBaseInstance.interceptors.request.use(
    request => {
        if (request.url.indexOf("/login") === -1) {
            request.headers['Authorization'] = sessionStorage.getItem("token");
        }
        return request
    },
    error => {
        return Promise.reject(error)
    });
//配置  http request 拦截器
axiosBaseInstanceJson.interceptors.request.use(
    request => {
        request.headers['Authorization'] = sessionStorage.getItem("token");
        return request
    },
    error => {
        return Promise.reject(error)
    });
//配置  http request 拦截器
axiosIMInstance.interceptors.request.use(
    request => {
        request.headers['Authorization'] = sessionStorage.getItem("token");
        return request
    },
    error => {
        return Promise.reject(error)
    });
//配置  http request 拦截器
axiosCommonInstance.interceptors.request.use(
    request => {
        request.headers['Authorization'] = sessionStorage.getItem("token");
        return request
    },
    error => {
        return Promise.reject(error)
    });
//配置  http request 拦截器
axiosFlowInstance.interceptors.request.use(
    request => {
        request.headers['Authorization'] = sessionStorage.getItem("token");
        return request
    },
    error => {
        return Promise.reject(error)
    });

//配置  http response 拦截器
axiosBaseInstance.interceptors.response.use(
    response => {
        //保存登录以后的token信息
        if (response.config.url.indexOf("/login") !== -1) {
            sessionStorage.setItem("token", response.headers.authorization);
        }
        return response;
    },
    error => {
        return Promise.reject(error);
    }
);

/**
 * 封装get方法
 * @param axiosInstance
 * @param url
 * @param params
 * @returns {Promise}
 */
export function $httpGet(axiosInstance, url, params = {}) {
    return new Promise((resolve, reject) => {
        axiosInstance.get(url, {
            params: params
        }).then(response => {
            resolve(response.data);
        }).catch(error => {
            _self.$errorMsg("网络请求错误");
            reject(error)
        })
    })
}

/**
 * 封装post请求
 * @param axiosInstance
 * @param url
 * @param data
 * @returns {Promise}
 */

export function $httpPost(axiosInstance, url, data = {}) {
    return new Promise((resolve, reject) => {
        axiosInstance.post(url, data)
            .then(response => {
                resolve(response.data);
            }).catch((error) => {
            _self.$errorMsg("网络请求错误");
            reject(error);
        });
    })
}


/**
 * 封装put请求
 * @param axiosInstance
 * @param url
 * @param data
 * @returns {Promise}
 */
export function $httpPut(axiosInstance, url, data = {}) {
    return new Promise((resolve, reject) => {
        axiosInstance.put(url, data)
            .then(response => {
                resolve(response.data);
            }).catch((error) => {
            _self.$errorMsg("网络请求错误");
            reject(error);
        });
    })
}

/**
 * 封装 delete 请求
 * @param axiosInstance
 * @param url
 * @param params
 * @returns {Promise}
 */
export function $httpDelete(axiosInstance, url, params = {}) {
    return new Promise((resolve, reject) => {
        axiosInstance.delete(url, {
            params: params
        }).then(response => {
            resolve(response.data);
        }).catch((error) => {
            _self.$errorMsg("网络请求错误");
            reject(error);
        })
    })
}

