import Vue from 'vue';
//1. 引入 Vuex
import Vuex from 'vuex';
//2.引入需要状态管理的组件js
import sysStore from "@/store/sys/sysStore";
import chatStore from "@/store/chat/chatStore";

//3.注册 Vuex 到vue中
Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        sysStore,
        chatStore
    }
});
