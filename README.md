# kola

#### 介绍（持续更新完善）
基于springboot 、vue实现的聊天功能，同时基于websocket的消息推送功能.和Activiti 6.0 流程引擎

#### 软件架构
* JDK 1.8+
* vue 2.x
* element-ui
* webscoket
* SpringBoot 2.x 
* Activiti 6.0.4 
* Mysql 6.0.6
* Maven 3.2.5
#### 后端运行依赖环境
* 依赖于nacos作为服务发现中心，并使用nginx服务器进行网关代理转发
#### 流程相关的系统截图
![流程模型设计](https://images.gitee.com/uploads/images/2020/0120/150946_7831ee9d_1101089.png "屏幕截图.png")
![流程模型管理](https://images.gitee.com/uploads/images/2020/0120/151056_e74fed7c_1101089.png "屏幕截图.png")
![查看模型流程图](https://images.gitee.com/uploads/images/2020/0120/153151_1786fdcf_1101089.png "屏幕截图.png")
![发起流程实例](https://images.gitee.com/uploads/images/2020/0120/153219_fd459015_1101089.png "屏幕截图.png")
![流程办理](https://images.gitee.com/uploads/images/2020/0120/153329_6301b649_1101089.png "屏幕截图.png")
![流程运行时流程图](https://images.gitee.com/uploads/images/2020/0120/153355_9f50da9c_1101089.png "屏幕截图.png")
#### 即时通讯相关的系统截图
![接收消息](https://images.gitee.com/uploads/images/2020/0120/154350_8571892d_1101089.png "屏幕截图.png")
![回复消息](https://images.gitee.com/uploads/images/2020/0120/154438_c2fbea53_1101089.png "屏幕截图.png")
![联系人列表](https://images.gitee.com/uploads/images/2020/0120/154608_a06eac18_1101089.png "屏幕截图")
![个人信息修改](https://images.gitee.com/uploads/images/2020/0120/154752_389b4046_1101089.png "屏幕截图.png")