package com.gateway.server.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：系统公共配置类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/6/25 10:14
 */
@Configuration
@ConfigurationProperties(prefix = "sys.config")
@Data
public class SysCommonConfig {

    private String nacosServerAddress;

    //服务调用url
    private Map<String, String> feignUrl = new HashMap<>();
    //服务地址，用于配置服务发布所在机器的域名
    private String serverUri;
    //系统类型
    private String sysType;

    //文件上传的路径
    private String uploadFilePath;

    //swagger2 开关
    private Boolean swagger2Enable;

    // Shiro缓存过期时间-5分钟-5*60(秒为单位)(一般设置与AccessToken过期时间一致)
    private String shiroCacheExpireTime;

    // AccessToken过期时间-5分钟-5*60(秒为单位)
    private String accessTokenExpireTime;

    // RefreshToken过期时间-30分钟-30*60(秒为单位)
    private String refreshTokenExpireTime;

    // AES密码加密私钥(Base64加密)
    private String encryptAESKey;

    // JWT认证加密私钥(Base64加密)
    private String encryptJWTKey;

}
