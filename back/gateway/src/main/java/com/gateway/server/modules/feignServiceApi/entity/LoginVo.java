package com.gateway.server.modules.feignServiceApi.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 登录信息
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
public class LoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 密码
     */
    private String password;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 记住我
     */
    private boolean rememberMe;
}
