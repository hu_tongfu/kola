package com.common.server.common.util;

import com.common.server.common.annotation.FeignClient;
import com.common.server.common.config.SysCommonConfig;
import feign.Feign;
import feign.Logger;
import feign.slf4j.Slf4jLogger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 描述：Feign 原生调用工具类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/1 9:32
 */
public class FeignUtil<T> {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private SysCommonConfig sysCommonConfig = (SysCommonConfig) ContextFactoryUtil.getBean("sysCommonConfig");

    /**
     * 解析自定义feign的注解
     *
     * @return url
     */
    private String url(Class<T> clzss) {
        FeignClient annotation = clzss.getAnnotation(FeignClient.class);
        if (annotation == null) {
            logger.error("该类没有指定注解");
            return null;
        }
        String name = clzss.getName();
        String key = name.substring(name.lastIndexOf(".") + 1);
        Map<String, String> feignUrl = sysCommonConfig.getFeignUrl();
        if (!feignUrl.containsKey(key)) {
            logger.error("配置文件中未配置[{}]", name);
            return null;
        }
        return feignUrl.get(key);
    }

//    public T invokeService(Class<T> vClass) {
//        Feign.Builder builder = Feign.builder().logger(new Slf4jLogger(vClass))
//                .logLevel(Logger.Level.FULL).client(new OkHttpClient())
//                .encoder(new JacksonEncoder()).decoder(new JacksonDecoder());
//        return builder.target(vClass, url(vClass));
//    }

}
