package com.common.server.common.response;


import com.common.server.common.constant.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：通用返回对象
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:36
 */
public class ResponseVo extends HashMap<String, Object> {

    private static final long serialVersionUID = 6199093063476482538L;

    public ResponseVo() {
        put("code", Constant.Result.SUCCESS_CODE);
    }

    public ResponseVo(int code, String msg) {
        put("code", code);
        put("msg", msg);
    }

    public ResponseVo(int code, String msg, Object data) {
        put("code", code);
        put("msg", msg);
        put("data", data);
    }

    public static ResponseVo error() {
        return new ResponseVo(Constant.Result.FAIL_CODE, Constant.Result.FAIL_MSG);
    }

    public static ResponseVo error(String msg) {
        return error(Constant.Result.FAIL_CODE, msg);
    }

    public static ResponseVo error(int code, String msg) {
        ResponseVo r = new ResponseVo();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static ResponseVo ok(String msg) {
        ResponseVo r = new ResponseVo();
        r.put("msg", msg);
        return r;
    }

    public static ResponseVo ok(String code, String msg) {
        ResponseVo r = new ResponseVo();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static ResponseVo ok(Map<String, Object> map) {
        ResponseVo r = new ResponseVo();
        r.putAll(map);
        return r;
    }

    public static ResponseVo ok() {
        return new ResponseVo(Constant.Result.SUCCESS_CODE, Constant.Result.SUCCESS_MSG);
    }

    public static ResponseVo ok(Object data) {
        return new ResponseVo(Constant.Result.SUCCESS_CODE, Constant.Result.SUCCESS_MSG, data);
    }

    public ResponseVo put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
