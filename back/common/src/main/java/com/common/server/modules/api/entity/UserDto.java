package com.common.server.modules.api.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author HuTongFu
 * @description: 用户
 * @since 2019/6/6 14:07
 */
@Data
public class UserDto implements Serializable {

    private static final long serialVersionUID = 9165600654133338745L;

    //用户id
    private String id;

    //姓名
    private String name;

    //登录名称
    private String loginName;

    //密码
    private String password;

    //旧密码
    private String oldPassword;

    //手机号
    private String phone;

    //邮箱
    private String email;

    //是否分配部门(1是0否)
    private Integer isHasDepart;

    //是否分配角色（1是0否）
    private Integer isHasRole;

    //头像url
    private String header;

    //性别
    private Integer gender;

    //用户类型（1超级管理员、2管理员、3普通用户）
    private String userType;

    //排序序号
    private Integer orderIndex;

    //状态
    private Integer status;

    //添加时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addDate;

    //修改时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate;

    //是否删除（1是、0否）
    private Integer isDelete;

    //修改人
    private Long updateUser;

    //添加人
    private Long addUser;

    //部门名称
    private String departName;
    private String departId;

    private List<Long> departIds;

    //角色名称
    private String roleName;
    private String roleId;

    //角色
    private List<Long> roleIds;

}
