package com.common.server.modules.dict.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_c_dict")
@ApiModel(value="Dict对象", description="字典表")
public class Dict extends Model<Dict> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "字典编码")
    private String code;

    @ApiModelProperty(value = "字典名称")
    private String value;

    @ApiModelProperty(value = "字典分类")
    private String type;

    @ApiModelProperty(value = "背景图片")
    private String backImg;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "是否删除")
    private Integer isDelete;

    @ApiModelProperty(value = "添加时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime addDate;

    @ApiModelProperty(value = "添加人")
    private String remark;

    @ApiModelProperty(value = "英文值")
    private String valueEn;

    @ApiModelProperty(value = "修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @ApiModelProperty(value = "序号")
    private String orderIndex;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
