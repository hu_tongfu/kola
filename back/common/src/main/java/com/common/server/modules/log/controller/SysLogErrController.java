package com.common.server.modules.log.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.server.common.response.ResponseVo;
import com.common.server.modules.log.entity.SysLogErr;
import com.common.server.modules.log.service.SysLogErrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 系统异常日志 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-10-09
 */
@Api(tags = "系统异常日志")
@RestController
@RequestMapping("api/rest/sysLogErr")
public class SysLogErrController {
    private final Logger logger = LoggerFactory.getLogger(SysLogErrController.class);

    @Autowired
    public SysLogErrService iSysLogErrService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public ResponseVo getSysLogErrList(SysLogErr sysLogErr) {
        try {
            QueryWrapper<SysLogErr> queryWrapper = new QueryWrapper<>(sysLogErr);
            List<SysLogErr> result = iSysLogErrService.list(queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("sysLogErrList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public ResponseVo getSysLogErrListPage(SysLogErr sysLogErr) {
        try {
            Page<SysLogErr> page = new Page<>();
            QueryWrapper<SysLogErr> queryWrapper = new QueryWrapper<>(sysLogErr);
            IPage<SysLogErr> result = iSysLogErrService.page(page, queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("getSysLogErrList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public ResponseVo sysLogErrSave(SysLogErr sysLogErr) {
        int count = 0;
        try {
            sysLogErr.setCreateTime(LocalDateTime.now());
            count = iSysLogErrService.save(sysLogErr) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogErrSave -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public ResponseVo sysLogErrUpdate(SysLogErr sysLogErr) {
        int count = 0;
        try {
            count = iSysLogErrService.save(sysLogErr) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogErrUpdate -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public ResponseVo sysLogErrDelete(@PathVariable String id) {
        int count = 0;
        try {
            count = iSysLogErrService.removeById(id) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogErrDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public ResponseVo deleteBatchIds(@RequestParam List<Long> ids) {
        int count = 0;
        try {
            count = iSysLogErrService.removeByIds(ids) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogErrBatchDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }
}

