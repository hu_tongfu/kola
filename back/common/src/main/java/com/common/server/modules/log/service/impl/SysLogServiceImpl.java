package com.common.server.modules.log.service.impl;

import com.common.server.modules.log.entity.SysLog;
import com.common.server.modules.log.mapper.SysLogMapper;
import com.common.server.modules.log.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-22
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
