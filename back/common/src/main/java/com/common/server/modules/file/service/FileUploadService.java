package com.common.server.modules.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.server.modules.file.entity.FileEntity;

/**
 * <p>
 * 文件上传 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-10-08
 */
public interface FileUploadService extends IService<FileEntity> {

}
