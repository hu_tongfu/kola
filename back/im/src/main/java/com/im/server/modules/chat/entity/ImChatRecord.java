package com.im.server.modules.chat.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天记录
 * </p>
 *
 * @author ostrich
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_im_chat_record")
@ApiModel(value = "ImChatRecord对象", description = "聊天记录")
public class ImChatRecord extends Model<ImChatRecord> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "消息发送人id")
    private String senderId;

    @ApiModelProperty(value = "消息内容")
    private String msgContent;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
