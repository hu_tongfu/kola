package com.im.server.modules.websocket.handler;

import com.im.server.common.mode.IModeHandleService;

/**
 * 描述：websocket 抽象处理器
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/12/17 14:38 星期二
 */
public abstract class AbstractWebSocketHandler implements IModeHandleService {
    public abstract boolean saveOrUpdateChatList(String event);
}
