package com.flow.server.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：系统自定义工作流异常
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysWorkflowException extends RuntimeException {

    private static final long serialVersionUID = 244180471316154668L;

    private String msg;
    private int code = 500;

    public SysWorkflowException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public SysWorkflowException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public SysWorkflowException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public SysWorkflowException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

}
