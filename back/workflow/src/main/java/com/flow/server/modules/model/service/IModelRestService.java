package com.flow.server.modules.model.service;

import com.flow.server.modules.base.service.IBaseService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 描述：流程模型 service
 * <p>
 * 作者：hutongfu
 * 时间：2020/1/18 11:19 星期六
 */
public interface IModelRestService extends IBaseService {

    /**
     * 查询模型列表
     *
     * @param request
     * @return
     */
    Map<String, Object> getModelList(HttpServletRequest request);

    /**
     * 部署流程模型
     *
     * @param modelId
     * @return
     */
    boolean deploymentModel(String modelId);

    /**
     * 保存模型
     *
     * @param request
     * @return
     */
    boolean saveModel(HttpServletRequest request);

    /**
     * 修改模型
     *
     * @param request
     * @param modelId
     * @return
     */
    boolean updateModel(HttpServletRequest request, String modelId);

    /**
     * 根据模型id获取流程模型静态流程图
     *
     * @param modelId
     * @param response
     */
    void getWorkflowImage(String modelId, HttpServletResponse response);
}
