package com.flow.server.modules.instance.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述: ActProcessInstanceEntity 流程实例
 * <p>
 * 作者: hu_to
 * 时间: 2020/06/22 10:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
//@TableName("t_operation_info")
public class ActProcessInstanceEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    //主键
    private String id;
    //流程名称
    private String name;
    //流程实例id
    private String instanceId;
    //流程定义id
    private String processDefId;
    //流程key
    private String key;
    //流程部署id
    private String deploymentId;
    //流程业务id
    private String businessKey;
    //流程发起人id
    private String startUserId;
    //流程发起人
    private String startUserName;
    //当前任务id
    private String currTaskId;
    //当前任务名称
    private String currTaskName;
    //当前任务定义key
    private String currTaskDefKey;
    //任务锁定时间
    private Date claimTime;
    //当前任务创建时间
    private Date currTaskCreateTime;
    //流程开始时间
    private Date startTime;
    //流程结束时间
    private Date endTime;
    //挂起/激活状态
    private int suspensionState;
    //流程实例活动状态
    private Boolean isActive;
    //流程分类
    private String category;
    //流程描述
    private String description;
}
