package com.flow.server.modules.instance.controller;

import com.flow.server.common.response.CommonResponse;
import com.flow.server.common.utils.PageUtil;
import com.flow.server.modules.base.controller.BaseController;
import com.flow.server.modules.cmd.BpmnBuilderUtil;
import com.flow.server.modules.cmd.ProcessInstanceCmd;
import com.flow.server.modules.instance.entity.ActProcessInstanceEntity;
import com.flow.server.modules.instance.entity.ActTaskHistoryEntity;
import com.flow.server.modules.instance.service.IInstanceRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 描述：流程实例前端控制器
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 9:12 星期日
 */
@Api(tags = "流程实例")
@RestController
@RequestMapping("api/rest/instance")
public class InstanceRestController extends BaseController {

    @Resource
    public TaskService taskService;
    @Resource
    private IInstanceRestService iInstanceRestService;

    @ApiOperation("发起流程实例")
    @PostMapping("/start")
    public CommonResponse startProcessInstance(HttpServletRequest request) {
        boolean flag = iInstanceRestService.startProcessInstance(request);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }

    @ApiOperation("分页查询运行中的流程实例")
    @GetMapping("/list/page")
    public CommonResponse getListPage(HttpServletRequest request) {
        Map<String, Object> result = iInstanceRestService.getListPage(request);
        return CommonResponse.ok(result);
    }

    @ApiOperation("分页查询我发起的流程实例")
    @GetMapping("/{uid}/start/list/page")
    public CommonResponse getMyStartInstanceListPage(HttpServletRequest request, @PathVariable String uid) {
        ProcessInstanceCmd processInstanceCmd = BpmnBuilderUtil.buildProcessInstanceCmd(request);
        processInstanceCmd.setStartUserId(uid);
        PageUtil<ActProcessInstanceEntity> result = iInstanceRestService.getInstanceListPage(processInstanceCmd);
        return CommonResponse.ok(result);
    }

    @ApiOperation("分页查询我待办的流程实例")
    @GetMapping("/{uid}/todo/list/page")
    public CommonResponse getMyTodoInstanceListPage(HttpServletRequest request, @PathVariable String uid) {
        ProcessInstanceCmd processInstanceCmd = BpmnBuilderUtil.buildProcessInstanceCmd(request);
        processInstanceCmd.setCurrUserId(uid);
        PageUtil<ActProcessInstanceEntity> result = iInstanceRestService.getInstanceListPage(processInstanceCmd);
        return CommonResponse.ok(result);
    }


    @ApiOperation("分页查询我已办的流程实例")
    @GetMapping("/{uid}/done/list/page")
    public CommonResponse getMyDoneInstanceListPage(@PathVariable String uid, HttpServletRequest request) {
        ProcessInstanceCmd processInstanceCmd = BpmnBuilderUtil.buildProcessInstanceCmd(request);
        processInstanceCmd.setCurrUserId(uid);
        PageUtil<ActProcessInstanceEntity> result = iInstanceRestService.getMyDoneInstancePage(processInstanceCmd);
        return CommonResponse.ok(result);
    }

    @ApiOperation("根据流程实例id获取运行时流程图")
    @GetMapping("/img/{instanceId}")
    public void getWorkflowImage(HttpServletResponse response, @PathVariable String instanceId) {
        iInstanceRestService.getRunWorkflowImage(instanceId, response);
    }

    @ApiOperation("查询流程实例审批历史信息")
    @GetMapping("/history/{instanceId}")
    public CommonResponse taskHistoryList(@PathVariable String instanceId) {
        List<ActTaskHistoryEntity> list = iInstanceRestService.getTaskHistoryList(instanceId);
        return CommonResponse.ok(list);
    }
}
