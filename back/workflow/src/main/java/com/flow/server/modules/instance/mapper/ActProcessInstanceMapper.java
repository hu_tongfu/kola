package com.flow.server.modules.instance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flow.server.modules.cmd.ProcessInstanceCmd;
import com.flow.server.modules.instance.entity.ActProcessInstanceEntity;
import com.flow.server.modules.instance.entity.ActTaskHistoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 描述: ActProcessInstanceMapper
 * <p>
 * 作者: hu_to
 * 时间: 2020/06/22 10:48
 */
public interface ActProcessInstanceMapper extends BaseMapper<ProcessInstanceCmd> {

    /**
     * 根据条件查询办件中心数据
     * v 1.0.0
     * hu_to 2020-6-22 上午 10:54
     *
     * @param page 参数
     * @return list
     */
    IPage<ActProcessInstanceEntity> selectInstanceListPage(Page<ProcessInstanceCmd> page,
                                                           @Param(Constants.WRAPPER) QueryWrapper<ProcessInstanceCmd> queryWrapper);

    /**
     * 查询流程任务历史信息
     * v 1.0.0
     * hu_to 2020-6-23 上午 10:31
     *
     * @param instanceId 流程实例id
     * @return list
     */
    List<ActTaskHistoryEntity> selectTaskHistoryList(String instanceId);

    /**
     * 查询我的已办流程实例信息
     * v 1.0.0
     * hu_to 2020-6-24 下午 2:55
     *
     * @param page 参数
     * @return list
     */
    IPage<ActProcessInstanceEntity> selectMyDoneInstancePage(Page<ProcessInstanceCmd> page,
                                                            @Param(Constants.WRAPPER) QueryWrapper<ProcessInstanceCmd> queryWrapper);
}
