package com.flow.server.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：FastDfs 异常
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/11/26 15:35 星期二
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FastDFSException extends RuntimeException {

    private static final long serialVersionUID = 244180471316154668L;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误消息
     */
    private String message;

    public FastDFSException() {
    }

    public FastDFSException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
