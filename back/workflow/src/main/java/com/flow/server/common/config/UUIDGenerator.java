package com.flow.server.common.config;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import org.activiti.engine.impl.cfg.IdGenerator;

/**
 * 描述：自定义 process 流程Id生成器
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/16 9:51
 */
public class UUIDGenerator implements IdGenerator {

    /**
     * 封装com.fasterxml.uuid.Generators的UUID, 通过Random数字生成, 中间无-分割.
     */
    private static String uuid() {
        return String.valueOf(Generators.timeBasedGenerator(EthernetAddress.fromInterface()).generate().timestamp());
    }

    @Override
    public String getNextId() {
        return UUIDGenerator.uuid();
    }
}
