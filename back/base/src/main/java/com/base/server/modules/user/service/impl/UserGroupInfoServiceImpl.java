package com.base.server.modules.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.modules.user.entity.UserGroupInfo;
import com.base.server.modules.user.mapper.UserGroupInfoMapper;
import com.base.server.modules.user.service.UserGroupInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户组表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class UserGroupInfoServiceImpl extends ServiceImpl<UserGroupInfoMapper, UserGroupInfo> implements UserGroupInfoService {

    @Autowired
    private UserGroupInfoMapper userGroupInfoMapper;

    @Override
    public IPage<UserGroupInfo> selectByPage(Page<UserGroupInfo> page, UserGroupInfo userGroupInfo) {
        QueryWrapper<UserGroupInfo> queryWrapper = new QueryWrapper<>(userGroupInfo);
        queryWrapper.like("name", userGroupInfo.getName());
        return userGroupInfoMapper.selectByPage(page, queryWrapper);
    }
}
