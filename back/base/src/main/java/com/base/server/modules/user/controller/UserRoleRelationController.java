package com.base.server.modules.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.base.server.modules.user.service.UserRoleRelationService;
import com.base.server.modules.user.entity.UserRoleRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
/**
 * <p>
 * 用户与角色关联表 前端控制器
 * </p>
 *
 * 作者: ostrich
 * 时间: 2019-12-16
 */
@Api(tags = "用户与角色关联表")
@RestController
@RequestMapping("api/rest/userRoleRelation")
public class UserRoleRelationController {
    private final Logger logger=LoggerFactory.getLogger(UserRoleRelationController.class);

    @Autowired
    public UserRoleRelationService iUserRoleRelationService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getUserRoleRelationList(UserRoleRelation userRoleRelation) {
        try {
            QueryWrapper<UserRoleRelation> queryWrapper = new QueryWrapper<>(userRoleRelation);
            List<UserRoleRelation> result = iUserRoleRelationService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getUserRoleRelationListPage(UserRoleRelation userRoleRelation){
        try {
            Page<UserRoleRelation> page = new Page<>();
            QueryWrapper<UserRoleRelation> queryWrapper = new QueryWrapper<>(userRoleRelation);
            IPage<UserRoleRelation> result = iUserRoleRelationService.page(page, queryWrapper);
        return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse userRoleRelationSave(UserRoleRelation userRoleRelation){
        try{
            userRoleRelation.setId(UuidUtil.randomUUID());
            iUserRoleRelationService.save(userRoleRelation);
            return CommonResponse.ok(userRoleRelation);
        }catch(Exception e){
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse userRoleRelationUpdate(UserRoleRelation userRoleRelation){
        try{
            iUserRoleRelationService.save(userRoleRelation);
            return CommonResponse.ok(userRoleRelation);
        }catch(Exception e){
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse userRoleRelationDelete(@PathVariable String id){
        try{
            int count = iUserRoleRelationService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids){
        try{
            int count = iUserRoleRelationService.removeByIds(ids)?1:0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("批量删除对象异常", e);
        }
     }
}

