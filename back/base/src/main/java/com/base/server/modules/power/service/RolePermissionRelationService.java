package com.base.server.modules.power.service;

import com.base.server.modules.power.entity.RolePermissionRelation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.server.modules.power.entity.RolePermissionRelationVo;

import java.util.List;

/**
 * <p>
 * 角色与权限关联表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RolePermissionRelationService extends IService<RolePermissionRelation> {

    void save(RolePermissionRelationVo param);

    List<RolePermissionRelation> getOne(String id);
}
