package com.base.server.modules.power.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.power.entity.PermissionInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.server.modules.power.entity.PermissionInfoVo;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface PermissionInfoService extends IService<PermissionInfo> {

    /**
     * 分页查询
     *
     * @param page
     * @param permissionInfoVo
     * @return
     */
    IPage<PermissionInfoVo> selectByMyPage(Page<PermissionInfoVo> page, PermissionInfoVo permissionInfoVo);
}
