package com.base.server.modules.notice.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.notice.entity.Notice;
import com.base.server.modules.notice.entity.NoticeVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 通知表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
public interface NoticeMapper extends BaseMapper<Notice> {

    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<NoticeVo> selectMyPage(Page<NoticeVo> page, @Param(Constants.WRAPPER) QueryWrapper<NoticeVo> queryWrapper);

    /**
     * 根据条件查询单条数据
     *
     * @param queryWrapper
     * @return
     */
    NoticeVo selectNoticeOne(@Param(Constants.WRAPPER) QueryWrapper<NoticeVo> queryWrapper);
}
