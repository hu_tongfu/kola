package com.base.server.modules.notice.service.impl;

import com.base.server.modules.notice.entity.NoticeReceiver;
import com.base.server.modules.notice.mapper.NoticeReceiverMapper;
import com.base.server.modules.notice.service.NoticeReceiverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息收件人表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
@Service
public class NoticeReceiverServiceImpl extends ServiceImpl<NoticeReceiverMapper, NoticeReceiver> implements NoticeReceiverService {

}
