package com.base.server.modules.operate.service;

import com.base.server.modules.operate.entity.OperationPermissionRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限与功能操作关联表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface OperationPermissionRelationService extends IService<OperationPermissionRelation> {

}
