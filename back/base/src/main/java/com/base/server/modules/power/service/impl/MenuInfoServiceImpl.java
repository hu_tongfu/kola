package com.base.server.modules.power.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.common.config.RedisClient;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.BuildTree;
import com.base.server.common.utils.MenuTree;
import com.base.server.common.utils.Tree;
import com.base.server.common.utils.UserUtils;
import com.base.server.modules.power.entity.MenuInfo;
import com.base.server.modules.power.entity.RolePermissionRelation;
import com.base.server.modules.power.mapper.MenuInfoMapper;
import com.base.server.modules.power.service.MenuInfoService;
import com.google.common.base.Strings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class MenuInfoServiceImpl extends ServiceImpl<MenuInfoMapper, MenuInfo> implements MenuInfoService {

    @Resource
    private MenuInfoMapper menuInfoMapper;

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public CommonResponse menuListByRole(MenuInfo param, String type) {
        List<MenuInfo> result = null;
        if ("list".equals(type)) {
            String roleId = UserUtils.getUserInfo().getRoleId();
            //查询出该用户角色拥有的菜单
            result = menuInfoMapper.menuListByRole(roleId.split(","));
            //过滤出拥有的菜单权限
            List<MenuTree<MenuInfo>> collect = result.parallelStream().map(m -> {
                MenuTree<MenuInfo> menuInfoMenuTree = new MenuTree<>();
                menuInfoMenuTree.setId(m.getId());
                menuInfoMenuTree.setPId(m.getParentId());
                menuInfoMenuTree.setPath(m.getUrl());
                menuInfoMenuTree.setIcon(m.getIcon());
                menuInfoMenuTree.setName(m.getName());
                menuInfoMenuTree.setOrder(String.valueOf(m.getOrderIndex()));
                return menuInfoMenuTree;
            }).collect(Collectors.toList());
            return CommonResponse.ok(BuildTree.buildMenuList(collect, "0"));
        } else if ("tree".equals(type)) {
            QueryWrapper<MenuInfo> queryWrapper = Wrappers.query(param);
            queryWrapper.orderByAsc("order_index");
            result = menuInfoMapper.selectList(queryWrapper);
            List<Tree<MenuInfo>> collect = result.parallelStream().map(m -> {
                Tree<MenuInfo> menuInfoMenuTree = new Tree<>();
                menuInfoMenuTree.setId(m.getId());
                menuInfoMenuTree.setParentId(m.getParentId());
                menuInfoMenuTree.setLabel(m.getName());
                menuInfoMenuTree.setIcon(m.getIcon());
                menuInfoMenuTree.setOrder(String.valueOf(m.getOrderIndex()));
                return menuInfoMenuTree;
            }).collect(Collectors.toList());

            return CommonResponse.ok(BuildTree.buildList(collect, "0"));
        } else {
            return CommonResponse.ok(result);
        }
    }
}
