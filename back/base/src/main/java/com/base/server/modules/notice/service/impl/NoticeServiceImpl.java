package com.base.server.modules.notice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.common.utils.UserUtils;
import com.base.server.modules.notice.entity.Notice;
import com.base.server.modules.notice.entity.NoticeAttachment;
import com.base.server.modules.notice.entity.NoticeReceiver;
import com.base.server.modules.notice.entity.NoticeVo;
import com.base.server.modules.notice.mapper.NoticeAttachmentMapper;
import com.base.server.modules.notice.mapper.NoticeMapper;
import com.base.server.modules.notice.mapper.NoticeReceiverMapper;
import com.base.server.modules.notice.service.NoticeService;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 通知表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Autowired
    private NoticeReceiverMapper noticeReceiverMapper;

    @Autowired
    private NoticeAttachmentMapper noticeAttachmentMapper;

    @Override
    public IPage<NoticeVo> selectByMyPage(Page<NoticeVo> page, NoticeVo noticeVo) {
        QueryWrapper<NoticeVo> queryWrapper = new QueryWrapper<>(noticeVo);
        if (StringUtils.isNoneBlank(noticeVo.getTitle()))
            queryWrapper.like("n.title", noticeVo.getTitle());
        if (Objects.nonNull(noticeVo.getStatus()))
            queryWrapper.eq("n.status", noticeVo.getStatus());
        if (Objects.nonNull(noticeVo.getTypes()))
            queryWrapper.eq("n.types", noticeVo.getTypes());
        if (StringUtils.isNoneBlank(noticeVo.getAddUser()))
            queryWrapper.like("u.name", noticeVo.getAddUser());
        queryWrapper.orderByDesc("n.add_time");
        IPage<NoticeVo> noticeVoIPage = noticeMapper.selectMyPage(page, queryWrapper);
        noticeVoIPage.setTotal(noticeVoIPage.getRecords().size());
        return noticeVoIPage;
    }

    @Override
    public NoticeVo selectNoticeOne(NoticeVo noticeVo) {
        QueryWrapper<NoticeVo> queryWrapper = new QueryWrapper<>(noticeVo);
        queryWrapper.eq("n.id", noticeVo.getId());
        return noticeMapper.selectNoticeOne(queryWrapper);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public NoticeVo save(NoticeVo noticeVo) {
        Notice notice = new Notice();
        BeanUtils.copyProperties(noticeVo, notice);
        notice.setAddUser(UserUtils.getUid());
        noticeMapper.insert(notice);
        List<NoticeAttachment> attachments = noticeVo.getAttachment();
        NoticeAttachment noticeAttachment = new NoticeAttachment();
        attachments.parallelStream().forEach(attachment -> {
            noticeAttachment.setNoticeId(notice.getId());
            noticeAttachment.setAttachmentId(attachment.getAttachmentId());
            noticeAttachment.setAttachmentName(attachment.getAttachmentName());
            noticeAttachmentMapper.insert(noticeAttachment);
        });
        List<NoticeReceiver> receivers = noticeVo.getReceiver();
        NoticeReceiver noticeReceiver = new NoticeReceiver();
        receivers.parallelStream().forEach(receiver -> {
            noticeReceiver.setNoticeId(notice.getId());
            noticeReceiver.setIsRead(false);
            noticeReceiver.setUserId(receiver.getUserId());
            noticeReceiverMapper.insert(noticeReceiver);
        });
        return noticeVo;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public NoticeVo noticeUpdate(NoticeVo noticeVo) {
        Notice notice = new Notice();
        notice.setId(noticeVo.getId());
        noticeMapper.deleteById(notice);
        Map<String, Object> column = Maps.newHashMap();
        column.put("notice_id", noticeVo.getId());
        noticeAttachmentMapper.deleteByMap(column);
        noticeReceiverMapper.deleteByMap(column);
        return save(noticeVo);
    }
}
