package com.base.server.modules.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.base.server.modules.user.service.UserGroupRelationService;
import com.base.server.modules.user.entity.UserGroupRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
/**
 * <p>
 * 用户组与用户关联表 前端控制器
 * </p>
 *
 * 作者: ostrich
 * 时间: 2019-12-16
 */
@Api(tags = "用户组与用户关联表")
@RestController
@RequestMapping("api/rest/userGroupRelation")
public class UserGroupRelationController {
    private final Logger logger=LoggerFactory.getLogger(UserGroupRelationController.class);

    @Autowired
    public UserGroupRelationService iUserGroupRelationService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getUserGroupRelationList(UserGroupRelation userGroupRelation) {
        try {
            QueryWrapper<UserGroupRelation> queryWrapper = new QueryWrapper<>(userGroupRelation);
            List<UserGroupRelation> result = iUserGroupRelationService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getUserGroupRelationListPage(UserGroupRelation userGroupRelation){
        try {
            Page<UserGroupRelation> page = new Page<>();
            QueryWrapper<UserGroupRelation> queryWrapper = new QueryWrapper<>(userGroupRelation);
            IPage<UserGroupRelation> result = iUserGroupRelationService.page(page, queryWrapper);
        return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse userGroupRelationSave(UserGroupRelation userGroupRelation){
        try{
            userGroupRelation.setId(UuidUtil.randomUUID());
            iUserGroupRelationService.save(userGroupRelation);
            return CommonResponse.ok(userGroupRelation);
        }catch(Exception e){
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse userGroupRelationUpdate(UserGroupRelation userGroupRelation){
        try{
            iUserGroupRelationService.save(userGroupRelation);
            return CommonResponse.ok(userGroupRelation);
        }catch(Exception e){
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse userGroupRelationDelete(@PathVariable String id){
        try{
            int count = iUserGroupRelationService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids){
        try{
            int count = iUserGroupRelationService.removeByIds(ids)?1:0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("批量删除对象异常", e);
        }
     }
}

