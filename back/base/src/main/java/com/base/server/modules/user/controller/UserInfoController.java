package com.base.server.modules.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.modules.user.entity.UserInfo;
import com.base.server.modules.user.entity.UserInfoVo;
import com.base.server.modules.user.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "用户表")
@RestController
@RequestMapping("api/rest/userInfo")
public class UserInfoController {
    private final Logger logger = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    public UserInfoService iUserInfoService;

    @ApiOperation("查询单个数据")
    @GetMapping(value = "/one")
    public CommonResponse getUserInfoOne(UserInfoVo userInfoVo) {
        try {
            UserInfoVo result = iUserInfoService.selectOne(userInfoVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getUserInfoList(UserInfoVo userInfoVo) {
        try {
            List<UserInfoVo> result = iUserInfoService.list(userInfoVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getUserInfoListPage(UserInfoVo userInfoVo,
                                              @RequestParam(defaultValue = "1") Long currentPage,
                                              @RequestParam(defaultValue = "10") Long pageSize) {
        try {
            Page<UserInfoVo> page = new Page<>(currentPage, pageSize);
            IPage<UserInfoVo> result = iUserInfoService.selectByMyPage(page, userInfoVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse userInfoSave(UserInfoVo userInfo) {
        try {
            iUserInfoService.save(userInfo);
            return CommonResponse.ok(userInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse userInfoUpdate(UserInfoVo userInfo) {
        try {
            return iUserInfoService.updateById(userInfo) ? CommonResponse.ok(userInfo) : CommonResponse.error();
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id修改用户信息")
    @PutMapping(value = "/upd/byOne")
    public CommonResponse userInfoUpdateByOne(UserInfoVo userInfo) {
        try {
            return iUserInfoService.userInfoUpdateByOne(userInfo) ? CommonResponse.ok(userInfo) : CommonResponse.error();
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }


    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse userInfoDelete(@PathVariable String id) {
        try {
            return iUserInfoService.removeById(id) ? CommonResponse.ok() : CommonResponse.error();
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iUserInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

