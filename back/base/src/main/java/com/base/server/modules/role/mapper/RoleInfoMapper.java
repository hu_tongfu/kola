package com.base.server.modules.role.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.role.entity.RoleInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {

    IPage<RoleInfo> selectByPage(Page<RoleInfo> page,@Param(Constants.WRAPPER) QueryWrapper<RoleInfo> queryWrapper);
}
