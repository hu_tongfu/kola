package com.base.server.modules.power.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.power.entity.PermissionInfo;
import com.base.server.modules.power.entity.PermissionInfoVo;
import com.base.server.modules.power.entity.RolePermissionRelation;
import com.base.server.modules.power.entity.RolePermissionRelationVo;
import com.base.server.modules.power.service.PermissionInfoService;
import com.base.server.modules.power.service.RolePermissionRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "权限表")
@RestController
@RequestMapping("api/rest/auth")
public class PermissionInfoController {
    private final Logger logger = LoggerFactory.getLogger(PermissionInfoController.class);

    @Resource
    private PermissionInfoService iPermissionInfoService;

    @Resource
    private RolePermissionRelationService rolePermissionRelationService;

    @ApiOperation("根据id查询")
    @GetMapping(value = "/one")
    public CommonResponse getPermissionInfoList(String id) {
        try {
            PermissionInfo byId = iPermissionInfoService.getById(id);
            return CommonResponse.ok(byId);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getPermissionInfoList(PermissionInfo permissionInfo) {
        try {
            QueryWrapper<PermissionInfo> queryWrapper = new QueryWrapper<>(permissionInfo);
            List<PermissionInfo> result = iPermissionInfoService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getPermissionInfoListPage(PermissionInfoVo permissionInfoVo,
                                                    @RequestParam(defaultValue = "1") Long currentPage,
                                                    @RequestParam(defaultValue = "10") Long pageSize) {
        try {
            Page<PermissionInfoVo> page = new Page<>(currentPage, pageSize);
            IPage<PermissionInfoVo> result = iPermissionInfoService.selectByMyPage(page, permissionInfoVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse permissionInfoSave(PermissionInfo permissionInfo) {
        try {
            permissionInfo.setId(UuidUtil.randomUUID());
            permissionInfo.setAddTime(LocalDateTime.now());
            iPermissionInfoService.save(permissionInfo);
            return CommonResponse.ok(permissionInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse permissionInfoUpdate(PermissionInfo permissionInfo) {
        try {
            iPermissionInfoService.saveOrUpdate(permissionInfo);
            permissionInfo.setUpdateTime(LocalDateTime.now());
            return CommonResponse.ok(permissionInfo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse permissionInfoDelete(@PathVariable String id) {
        try {
            int count = iPermissionInfoService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iPermissionInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }

    @ApiOperation("新增角色与权限关联关系")
    @PostMapping(value = "/role/save")
    public CommonResponse roleAuthSave(RolePermissionRelationVo param) {
        try {
            rolePermissionRelationService.save(param);
            return CommonResponse.ok(param);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("根据角色id查询出拥有的权限")
    @GetMapping(value = "/role/{id}")
    public CommonResponse roleWithAuth(@PathVariable String id) {
        try {
            List<RolePermissionRelation> byId = rolePermissionRelationService.getOne(id);
            return CommonResponse.ok(byId);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }
}

